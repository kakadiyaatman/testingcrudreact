import React, { Component } from "react";
import "./App.scss";

import Stepper from "./Stepper";
import Logo from "../src/Assets/Logo.png";
import Step1 from "./Assets/Wizard-Step1.png";
import Step2 from "./Assets/Wizard-Step2.png";
import Step3 from "./Assets/Wizard-Step3.png";
import Step4 from "./Assets/Wizard-Step4.png";
import Step5 from "./Assets/Wizard-Step5.png";
import CreateAcccount from "./Component/CreateAcccount";
import LoginAccount from "./Component/LoginAccount"
import UserDetail from "./Component/UserDetail";
import UpdateDetail from "./Component/UpdateDetail";
import DeleteUser from "./Component/DeleteUser";

const steps = [
  {
    logo: Step1,
    label1 :"Step 1:",
    label2: "Create Your  Account Password",
    component: <CreateAcccount />,
    exitValidation: false,
  },
  {
    logo: Step2,
    label1 :"Step 2:",
    label2: "Account Login",
    component: <LoginAccount />,
  },
  {
    logo: Step3,
    label1 :"Step 3:",
    label2: "Employment Details",
    component: <UserDetail />,
  },
  {
    logo: Step4,
    label1 :"Step 4:",
    label2: "Upload Document",
    component: <UpdateDetail />,
  },
  {
    logo: Step5,
    label1 :"Step 5:",
    label2: "Complete",
    component:<DeleteUser />,
  },
];

function App(props) {
  
  return (
    <div className="App">
      <div>
        <img src={Logo} />
      </div>
      <div>
        <Stepper steps={steps} />
      </div>
    </div>
  );
}

export default App;
