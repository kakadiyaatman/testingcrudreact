import React, { Component } from "react";
import "./App.scss";

import linkage from "./Assets/Wizard-HorizontalBar.png";

export default class Stepper extends Component {
  constructor() {
    super();

    this.state = { activeStep: 0 };
  }

  handleStepChange(activeStep) {
    this.setState({ activeStep });
  }

  nextStep() {
    if (this.state.activeStep < this.props.steps.length - 1) {
      this.setState({ activeStep: this.state.activeStep + 1 });
    }
  }
   logout(){
    localStorage.clear();
    alert("Logout successfully")

  }

  previousStep() {
    if (this.state.activeStep > 0) {
      this.setState({ activeStep: this.state.activeStep - 1 });
    }
  }

  render() {
    const { steps, onFinish } = this.props;
    const { activeStep } = this.state;
    const stepIndicators = steps.map((step, i) => {
      return (
        <div className="stepper-inner" >
          <div className={`stepper-number ${activeStep === i && "active"}`}>
            <span>{i + 1}</span>

            {/* <div className="line-linkage"> <img src={linkage} /></div> */}
          </div>
          {i !== steps.length && (
            <div className="stepper-logo">
              {" "}
              {/* <img className="stepper-log" src={step.logo} /> */}
              <div className="stepper-label1">{step.label1}</div>
              <div className="stepper-label2">{step.label2}</div>
            </div>
          )}
        </div>
      );
    });


    return (
      <div className="stepper">
        <div className="stepper-indicator">{stepIndicators}</div>
        <div className="stepper-steps">{steps[activeStep].component}</div>
        <div className="stepper-actions">
         {activeStep<1 ? null:  <button className="prev-btn" onClick={() => this.previousStep()}>Previous</button>} 
      
          {activeStep === steps.length - 1 ? localStorage.getItem("token") ? <button
             
              onClick={() => this.logout()}
            >
               Logout
            </button>:null
           : 
         <button
            disabled={!!steps[activeStep].exitValidation}
            onClick={() => this.nextStep()}
          >
             Next
          </button>

           
           
          }
        </div>
      </div>
    );
  }
}
