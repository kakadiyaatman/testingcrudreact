import React from "react";
import axios from "axios";

export default function DeleteUser() {


    const UserDelete=()=>{
    const token = JSON.parse(localStorage.getItem("token"));
    const tkn = "bearer" + " " + token;
    const data = { Authorization: tkn };
    
    var config = {
        method: "post",
        url: "http://localhost:5000/user/deleteUser",
        headers: data,
      };
      
      if(localStorage.getItem("token"))
      {
        axios(config)
        .then((resp) => {
          alert(resp.data.message);
          localStorage.clear();
    
    
        })
        .catch((err) => {
          alert(err);
        });
      }
    



    }

  return (localStorage.getItem("token") ?
    <div>
      <div className="save-btn">
        <button onClick={()=>UserDelete()}>Delete User</button>
      </div>
    </div> : <p>
      <h3>You Have To Login First....</h3>
    </p>
  );
}
