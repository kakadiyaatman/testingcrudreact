import React, { useState } from "react";
import Avatar from "../Assets/Avatar.png";
import "../App.scss";
import axios from "axios";
import {
  Button,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
} from "reactstrap";

export default function CreateAcccount() {
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [image, setImage] = useState();
  const [password, setPassword] = useState();
  const [confirmpass, setConfirmpass] = useState();
  const [errors, setErrors] = useState({});
  const hiddenFileInput = React.useRef(null);
  const [save, setSave] = useState(true);
  // const Formdata ={name:name ,email:email,password:password,attachment:image}
  let formData = new FormData();

  formData.append("name", name);
  formData.append("email", email);
  formData.append("password", password);
  formData.append("attachment", image);

  const config = {
    headers: { "content-type": "multipart/form-data" },
  };

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;
    let doc = document.getElementById("input");
    if (!email) {
      formIsValid = false;
      errors["email"] = "*Please Enter Email";
    }
    if (!name) {
      formIsValid = false;
      errors["name"] = "*Please Enter  Name";
    }

    if (!image) {
      formIsValid = false;
      errors["file"] = "*Please Upload Your Photo";
    }
    if (!password) {
      formIsValid = false;
      errors["password"] = "*Please Valid Password";
    }
    if (password !== confirmpass) {
      formIsValid = false;
      errors["confirmpass"] = "*Please Valid Confirm Password";
    }

    setErrors(errors);
    return formIsValid;
  };
  const submitData = () => {
    if (validateForm()) {
      axios
        .post("http://localhost:5000/user/userSignUp", formData, config)
        .then((resp) => {
         
          alert(resp.data.message)
          
        })
        .catch((err) => {
          alert(err);
        });
        setSave(!save);
        setName("");
        setEmail("");
        setPassword("");
        setConfirmpass("");
        setImage("");
    }
    
  };

  const handleClick = (e) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (e) => {
    setImage(e.target.files[0]);
  };

  return (
    <div className="Container">
      <div>
        <div className="form-label">
          {" "}
          <h4>CREATE YOUR ACCOUNT</h4>
        </div>
        <div>
          {" "}
          <h6>
            because there will be documents that your need to prepare to apply
            for the loan,let's start off by creating a password so that you can
            login to your account once you have these document ready.
          </h6>
        </div>
        <div className="form-group">
          <div onClick={(e) => handleClick(e)} className="upload-img">
            <img src={Avatar} />
            <input
              type="file"
              id="input"
              ref={hiddenFileInput}
              onChange={(e) => handleChange(e)}
              style={{ display: "none" }}
            />
            <span
              style={{
                color: "red",

                top: "5px",
                fontSize: "12px",
              }}
            >
              {errors["file"]}
            </span>

            <span className="upload-text">Upload</span>
          </div>
          <div>
            <Form>
              <FormGroup>
                <Col sm={7}>
                  <Label for="exampleEmail">Name</Label>

                  <Input
                    type="name"
                    name="name"
                    id="input"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                  <span
                    style={{
                      color: "red",

                      top: "5px",
                      fontSize: "12px",
                    }}
                  >
                    {errors["name"]}
                  </span>
                </Col>
                <Col sm={1}></Col>
                <Col sm={7}>
                  <Label for="examplePassword">Email</Label>
                  <Input
                    type="email"
                    name="email"
                    id="input"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />{" "}
                  <span
                    style={{
                      color: "red",

                      top: "5px",
                      fontSize: "12px",
                    }}
                  >
                    {errors["email"]}
                  </span>
                </Col>
              </FormGroup>
              <FormGroup>
                <Col sm={7}>
                  <Label for="exampleUrl">Password</Label>
                  <Input
                    type="password"
                    name="password"
                    id="input"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <span
                    style={{
                      color: "red",

                      top: "5px",
                      fontSize: "12px",
                    }}
                  >
                    {errors["password"]}
                  </span>
                </Col>
                <Col sm={1}></Col>
                <Col sm={7}>
                  <Label for="exampleNumber">Confirm Password</Label>
                  <Input
                    type="password"
                    name="confirmpassword"
                    id="input"
                    value={confirmpass}
                    onChange={(e) => setConfirmpass(e.target.value)}
                  />
                  <span
                    style={{
                      color: "red",

                      top: "5px",
                      fontSize: "12px",
                    }}
                  >
                    {errors["confirmpass"]}
                  </span>
                </Col>
              </FormGroup>
            </Form>
          </div>
        </div>
      </div>
      {save ? (
        <div className="save-btn">
          <button onClick={() => submitData()}>Save</button>
        </div>
      ) : null}
    </div>
  );
}
