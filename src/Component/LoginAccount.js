import React, { useState,useEffect } from "react";
import "../App.scss";
import axios from "axios";
import { Col, Form, FormGroup, Label, Input } from "reactstrap";

export default function LoginAccount() {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [errors, setErrors] = useState({});

  const [save, setSave] = useState(true);
  const logindata = { email: email, password: password };

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;
    let doc = document.getElementById("input");
    if (!email) {
      formIsValid = false;
      errors["email"] = "*Please Enter Email";
    }
    if (!password) {
      formIsValid = false;
      errors["password"] = "*Please Enter Password";
    }

    setErrors(errors);
    return formIsValid;
  };
  const submitData = () => {
    if (validateForm()) {
      axios
        .post("http://localhost:5000/user/userLogin", logindata)
        .then((resp) => {
         alert(resp.data.message);
         localStorage.setItem("userinfo",JSON.stringify(resp.data))
         localStorage.setItem("token",JSON.stringify(resp.data.token))

        })
        .catch((err) => {
          alert(err);
        });
      setSave(!save);
    }
   
  };
  

  return (
    <div className="Container">
      <div>
        <div className="info-label">
          {" "}
          <h4>Login to Your Acccount</h4>
        </div>

        <div className="login-form">
          <Form>
            <FormGroup className="form">
              <Col sm={5}>
                <Label for="exampleUrl">Enter Your Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="input"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <span
                  style={{
                    color: "red",

                    top: "5px",
                    fontSize: "12px",
                  }}
                >
                  {errors["email"]}
                </span>
              </Col>
            </FormGroup>
            <FormGroup className="form">
              <Col sm={5}>
                <Label for="exampleUrl">Enter Your Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="input"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <span
                  style={{
                    color: "red",

                    top: "5px",
                    fontSize: "12px",
                  }}
                >
                  {errors["password"]}
                </span>
              </Col>
            </FormGroup>
          </Form>
        </div>
      </div>
      {save ? (
        <div className="login-btn">
          <button onClick={() => submitData()}>Login</button>
        </div>
      ) : null}
    </div>
  );
}
