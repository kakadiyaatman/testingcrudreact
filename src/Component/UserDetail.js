import React, { useState, useEffect } from "react";
import "../App.scss";
import axios from "axios";
import {
  Button,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
} from "reactstrap";

export default function UserDetail() {
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [image, setImage] = useState();

  const [save, setSave] = useState(true);

  useEffect(() => {
    const token = JSON.parse(localStorage.getItem("token"));
    const tkn = "bearer" + " " + token;
    const data = { Authorization: tkn };

    var config = {
      method: "post",
      url: "http://localhost:5000/user/getUserDetails",
      headers: data,
    };
    if (localStorage.getItem("token")) {
      axios(config)
        .then((resp) => {
          setName(resp.data.data.name);
          setEmail(resp.data.data.email);
          setImage(resp.data.data.avimg);
        })
        .catch((err) => {
          alert(err);
        });
    }
  }, []);

  return localStorage.getItem("token") ? (
    <div className="Container">
      <div>
        <div className="info-label">
          {" "}
          <h4> Your Acccount Details</h4>
        </div>
        <div className="user-detail">
          <label>
            <h5>Name :- </h5>
          </label>
          <h5>
            <span>{name ? name : null}</span>
          </h5>
        </div>
        <div className="user-email">
          <label>
            <h5>Email :- </h5>
          </label>
          <h5>
            <span>{email ? email : null}</span>
          </h5>
        </div>
      </div>
    </div>
  ) : (
    <p>
      <h3>You Have To Login First....</h3>
    </p>
  );
}
