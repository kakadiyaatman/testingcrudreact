import React, { useState, useEffect } from "react";
import "../App.scss";
import axios from "axios";
import Avatar from "../Assets/Avatar.png";
import {
  Button,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
} from "reactstrap";

export default function UpdateDetail() {
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [image, setImage] = useState();
  const hiddenFileInput = React.useRef(null);
  const [errors, setErrors] = useState({});

  const [save, setSave] = useState(true);

  useEffect(() => {
    const token = JSON.parse(localStorage.getItem("token"));
    const tkn = "bearer" + " " + token;
    const data = { Authorization: tkn };

    var config = {
      method: "post",
      url: "http://localhost:5000/user/getUserDetails",
      headers: data,
    };
    if (localStorage.getItem("token")) {
      axios(config)
        .then((resp) => {
        
          setName(resp.data.data.name);
          setEmail(resp.data.data.email);
          // setImage(resp.data.data.avimg);
        })
        .catch((err) => {
          alert(err);
        });
    }
  }, []);
  const handleClick = (e) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (e) => {
    setImage(e.target.files[0]);
  };

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;
    let doc = document.getElementById("input");
    if (!email) {
      formIsValid = false;
      errors["email"] = "*Please Enter Email";
    }
    if (!name) {
      formIsValid = false;
      errors["name"] = "*Please Enter  Name";
    }

    // if (!image) {
    //   formIsValid = false;
    //   errors["file"] = "*Please Upload Your Photo";
    // }

    setErrors(errors);
    return formIsValid;
  };
  const Editdata = () => {
    if (validateForm()) {
      // const edit={email:email,name:name,attachment:image}
      const token = JSON.parse(localStorage.getItem("token"));
      const tkn = "bearer" + " " + token;

      let data = new FormData();
      data.append("name", name);
     if(image){data.append("attachment", image)}
      const headers = {
        Authorization: tkn,
      };
      var config = {
        url: "http://localhost:5000/user/updateUserDetails",
      };
      axios
        .patch("http://localhost:5000/user/updateUserDetails", data, {
          headers,
        })
        .then((response) => {})
        .catch((error) => {});

  
        setSave(!save);
    }
  };

  return (localStorage.getItem("token") ?
    <div className="Container">
      <div>
        <div className="info-label">
          {" "}
          <h4>Edit Your Acccount Details</h4>
        </div>
        <div className="form-group">
          <div onClick={(e) => handleClick(e)} className="upload-img">
            <img src={Avatar} />
            <input
              type="file"
              id="input"
              ref={hiddenFileInput}
              onChange={(e) => handleChange(e)}
              style={{ display: "none" }}
            />
            <span
              style={{
                color: "red",

                top: "5px",
                fontSize: "12px",
              }}
            >
              {errors["file"]}
            </span>

            <span className="change-text">Change Photo </span>
          </div>
          <div className="Edit-form">
            <Form>
              <FormGroup className="Edit-details">
                <Col className="form" lg={6}>
                  <Label for="exampleEmail">Edit Name</Label>
                </Col>
                <Col lg={10}>
                  <Input
                    type="name"
                    name="name"
                    id="input"
                    value={name ? name : null}
                    onChange={(e) => setName(e.target.value)}
                  />
                  <span
                    style={{
                      color: "red",

                      top: "5px",
                      fontSize: "12px",
                    }}
                  >
                    {errors["name"]}
                  </span>
                </Col>
              </FormGroup>
              <FormGroup>
                <Col className="form" sm={6}>
                  <Label for="examplePassword">Edit Email</Label>
                </Col>
                <Col className="form" sm={10}>
                  <Input
                    type="email"
                    name="email"
                    id="input"
                    value={email ? email : null}
                    onChange={(e) => setEmail(e.target.value)}
                  />{" "}
                  <span
                    style={{
                      color: "red",

                      top: "5px",
                      fontSize: "12px",
                    }}
                  >
                    {errors["email"]}
                  </span>
                </Col>
              </FormGroup>
            </Form>
          </div>
        </div>
      </div>
      {save ? (
        <div className="save-btn">
          <button onClick={() => Editdata()}>Edit Details</button>
        </div>
      ) : null}
    </div>:  <p>
      <h3>You Have To Login First....</h3>
    </p>
  );
}
